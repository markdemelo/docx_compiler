from setuptools import setup, find_packages

with open("README.md") as readme_file:
    README = readme_file.read()

VERSION = "0.0.1"
DESCRIPTION = "Combine Word (.docx) files and save backup"
LONG_DESCRIPTION = README

setup(
    name="docx_composer",
    version=VERSION,
    author="Mark de Melo",
    email="mark.de-melo@arup.com",
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    url="",
    license="MIT",
    packages=find_packages(),
    package_dir={"": "src"},
    install_requires=[
        "docxcompose",
    ],
    extras_require={},
)
