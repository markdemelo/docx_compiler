# docx_compiler

Compile several word files (.docx) into a single document, copying all 
formatting, images, tables and references

https://pypi.org/project/docxcompose/