#  --------------------------------------
#  Created on 2020-09-21 21:59:00
#  Author: Mark de Melo
#  --------------------------------------
# Description: Combine Word (.docx) files and save backup

import os
import time
from datetime import datetime

from docx import Document
from docxcompose.composer import Composer


def docx_lists(path):
    """create a list of docx files to combine and
    a list of docx files to append"""
    files_to_combine = []
    print("Creating lists to combine the following .docx: ")
    for file in os.listdir(path):
        if file.endswith(".docx"):
            files_to_combine.append(file)
            print(f"\t{file}")
    files_to_append = files_to_combine[1:]
    print(
        f"\nStart with: '{files_to_combine[0]}'." f"\nAppend with: {files_to_append}.\n"
    )
    return files_to_combine, files_to_append


def combine_docx(files_to_combine, files_to_append, path, output_path):
    """cycle through files_to_append appending each starting from
    the first file in list(files)"""
    combined_file = f"9V9589-40-GE-00-WP1-DO-122488.docx"
    for i, file in enumerate(files_to_append):
        if i == 0:
            master = Document(os.path.join(path, files_to_combine[0]))
            composer = Composer(master)
            file_to_add = Document(os.path.join(path, file))
            composer.append(file_to_add)
        else:
            master = Document(os.path.join(output_path, combined_file))
            composer = Composer(master)
            file_to_add = Document(os.path.join(path, file))
            composer.append(file_to_add)
        composer.save(os.path.join(output_path, combined_file))
        print(f"Appended '{file}'")
    print(f"Combined file: {combined_file}\nSaved to: {output_path}\n")
    return composer


def save_backup(file, output_path):
    """save backup file with timestamp"""
    time_stamp = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    backup_file = f"9V9589-40-GE-00-WP1-DO-122488_{time_stamp}.docx"
    file.save(os.path.join(output_path, backup_file))
    print(f"Backup file: '{backup_file}'\nSaved to: {output_path}\n")


def script_timer(start_time):
    """ calculates and prints script running time """
    execution_time = time.time() - start_time
    print(f"Document combined in: {round(execution_time, 2)} s")


if __name__ == "__main__":
    # Combine all files in path

    # define start time and paths to docx files to combine
    time_start = time.time()

    # define location of files and directories to save combined document
    path = r"F:\04 Internal Project Data\05 Reports\WP1_New_Bridge\design_report_v2_DO"
    output_path = fr"{path}\combined_docx"
    backup_path = fr"{path}\backup_docx"

    # Combine files, save and create timestamped backup
    files_to_combine, files_to_append = docx_lists(path)
    combined_docx = combine_docx(files_to_combine, files_to_append, path, output_path)
    save_backup(combined_docx, backup_path)
    script_timer(time_start)
